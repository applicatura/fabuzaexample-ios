# Тестовое приложение для подлючения фрэймворка SCKit.framework

Подключение фрэймворка:

1/ Перетащить фрэймворк SCKit.framework в проект
2/ В таргете на закладке Build Phases добавить фрэймворк в раздел Embed Framework
3/ В настройках проекта выставить параметр "Embedded content contains swift code" в YES
4/ В Info.plist проекта добавить разрешение на передачу по сети:
    <key>NSAppTransportSecurity</key>
    <dict>
        <key>NSAllowsArbitraryLoads</key>
        <true/>
    </dict>

5/ Если у приложения нет уникальной URL схемы, то ее нужно создать:
<key>CFBundleURLTypes</key>
<array>
    <dict>
    <key>CFBundleURLName</key>
    <string>com.application.fabuzaExample</string>
    <key>CFBundleURLSchemes</key>
    <array>
        <string>fabuzaExample</string>
    </array>
    </dict>
</array>


6/ Поскольку фрэймворк собран универсальным и для симулятора и для телефона, то перед отправкой в аппстор из него нужно удалить архитектуру симулятора. Это делает нижеследующий скрипт, который нужно в настройках таргета, на закладке Build Phases добавить, как "New run script phase":

APP_PATH="${TARGET_BUILD_DIR}/${WRAPPER_NAME}"

# This script loops through the frameworks embedded in the application and
# removes unused architectures.
find "$APP_PATH" -name '*.framework' -type d | while read -r FRAMEWORK
do
FRAMEWORK_EXECUTABLE_NAME=$(defaults read "$FRAMEWORK/Info.plist" CFBundleExecutable)
FRAMEWORK_EXECUTABLE_PATH="$FRAMEWORK/$FRAMEWORK_EXECUTABLE_NAME"
echo "Executable is $FRAMEWORK_EXECUTABLE_PATH"

EXTRACTED_ARCHS=()

for ARCH in $ARCHS
do
echo "Extracting $ARCH from $FRAMEWORK_EXECUTABLE_NAME"
lipo -extract "$ARCH" "$FRAMEWORK_EXECUTABLE_PATH" -o "$FRAMEWORK_EXECUTABLE_PATH-$ARCH"
EXTRACTED_ARCHS+=("$FRAMEWORK_EXECUTABLE_PATH-$ARCH")
done

echo "Merging extracted architectures: ${ARCHS}"
lipo -o "$FRAMEWORK_EXECUTABLE_PATH-merged" -create "${EXTRACTED_ARCHS[@]}"
rm "${EXTRACTED_ARCHS[@]}"

echo "Replacing original executable with thinned version"
rm "$FRAMEWORK_EXECUTABLE_PATH"
mv "$FRAMEWORK_EXECUTABLE_PATH-merged" "$FRAMEWORK_EXECUTABLE_PATH"

done

7/ В AppDelegate.h вставить:

#import <SCKit/SCKit.h>

и два свойства
@property (strong, nonatomic) FZTouchVisualizerWindow *window;
@property (nonatomic) NSURL *externalUrl;

8/ В AppDelegate.m вставить:

@interface AppDelegate () <FZTestEngineDataSource, FZTestEngineDelegate>

@property (nonatomic) FZTestEngine *testEngine;

@end

- (FZTouchVisualizerWindow *)window {
    static FZTouchVisualizerWindow *customWindow = nil;

    if (!customWindow) {
    customWindow = [[FZTouchVisualizerWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    }

    return customWindow;
}

//В iOS9 нужно использовать
//- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary *)options {
    self.externalUrl = url;

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [self.window pauseRecord];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    if (self.externalUrl) {
        [self initSCKit];
        [self.window resumeRecord];
    } else {
        [self openFabuzaWithParams:@{@"bundleId" : [[NSBundle mainBundle] bundleIdentifier]}];
    }
}

#pragma mark - SCKit initialization

- (void)openFabuzaWithParams:(NSDictionary *)params {
    [FZTestEngine instance].dataSource = self;
    [FZTestEngine instance].delegate = self;
    [[FZTestEngine instance] openFabuzaWithParams:params];
}

- (void)initSCKit {
    [FZTestEngine instance].dataSource = self;
    [FZTestEngine instance].delegate = self;
    [[FZTestEngine instance] on];
}

#pragma mark - FZTestEngineDataSource

- (NSURL *)getExternalUrl {
    return self.externalUrl;
}

- (NSUInteger)getVideoFilesSize {
    return self.window.videoSize;
}

#pragma mark - FZTestEngineDelegate

- (void)startRecordScreen:(BOOL)screenRecord andCamera:(BOOL)cameraRecord {
    //Для проектов использующих камеру, запись теста с камеры надо выключать так andCamera:NO
    [self.window startRecordScreen:screenRecord andCamera:YES];
}

- (void)stopRecordWithProgress:(void (^)(NSProgress *progress))progress
success:(void (^)(NSString *pathToScreenFile, NSString *pathToCameraFile))success
failure:(void (^)(NSError *error))failure {
    [self.window stopRecordWithProgress:progress success:success failure:failure];
}

- (void)didEndProcess {
    NSDictionary *params = [self.testEngine parseExternalTestParamsFromUrl:self.externalUrl];
    [self openFabuzaWithParams:params];
}
